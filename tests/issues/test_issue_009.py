import related
import pytest
import yaml


@related.immutable
class Child(object):
    name = related.StringField()
    values = related.SequenceField(str, required=False)


@related.immutable
class Root(object):
    children = related.MappingField(Child, 'name', required=False)
    another = related.MappingField(Child, 'name', default={})


def test_empty_mapping():
    root = related.from_yaml(stream=INPUT_YAML, yaml_package=yaml, cls=Root, loader_cls=yaml.Loader)
    assert related.to_yaml(obj=root, yaml_package=yaml, dumper_cls=yaml.Dumper).strip() == OUTPUT_YAML


def test_null_mapping():
    root = related.from_yaml(stream=OUTPUT_YAML, yaml_package=yaml, cls=Root, loader_cls=yaml.Loader)
    assert related.to_yaml(obj=root, yaml_package=yaml, dumper_cls=yaml.Dumper).strip() == OUTPUT_YAML


def test_invalid_mapping():
    with pytest.raises(TypeError):
        related.from_yaml(stream=INVALID_YAML, yaml_package=yaml, cls=Root, loader_cls=yaml.Loader)


INPUT_YAML = "children:"
OUTPUT_YAML = "children: {}\nanother: {}"
INVALID_YAML = "children: 5"
