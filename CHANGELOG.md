# related versions:

1.1.0 (2023-07-27):
------------------
Replace the usage of OrderedDict by dict

1.0.2 (2023-02-03):
------------------
Fix issue with newer attrs package versions
- Don't use protected iteritems function from attr._compat

1.0.1 (2022-06-29):
------------------
Prepare package for PyPi:
- Update copyright information
- Fix package description for pypi.org
- Use poetry as build and dependency management system

1.0.0 (2020-12-07):
------------------
- create fork
- apply initial modifications:
  - add possibility to inject yaml package via method parameter
  - add fhg gitlab stages
  - ensure that the __call__ method of attribute classes can be used
    when the ChildConverter.__call__ is invoked

0.7.1 (2018-10-13)
------------------
- Add URL to related pypi page [#28]
- Make singledispatch an optional dependency for < python 3.4. Thanks [GhostofGoes].
- URLField bug [#20]
- Deprecation warnings fixed for python 3.7 [#27]


0.6.2 (2018-02-12)
----------------
- Contribution [GabrielDav]: TimeField and DateTimeField fields.


0.6.1 (2018-01-31)
----------------
- Strict Mode [Issue #8] throws an exception when receiving an undefined key.


0.3 (2017-06-23)
----------------
- New type: ImmutableDict
- Add function on TypedMapping
- Bug fixes in from_yaml and from_json functions.


0.2 (2017-06-05)
----------------
- Allow None by default in Typed Collections.


0.1 (2017-05-24)
----------------
- Initial release.
